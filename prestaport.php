<?php


if (!defined('_PS_VERSION_'))
    exit;

class PrestaPort extends Module
{
    public function __construct()
    {
        $this->name = 'ShopifyToPrestashop';
        $this->tab = 'migration_tools';
        $this->version = '0.0.1';
        $this->author = 'Prestashop';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('ShopifyToPrestashop');
        $this->description = $this->l('transfer data from shopify to prestashop');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MYMODULE_NAME'))
            $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
        !$this->registerHook('leftColumn') ||
        !$this->registerHook('header') ||
        !Configuration::updateValue('MYMODULE_NAME', 'my friend')
        )
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

}

?>